package fr.ulille.iut.todo.dto;

public class CreationTacheDTO {
    private String nom;
    private String description;

    public CreationTacheDTO() {}

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
